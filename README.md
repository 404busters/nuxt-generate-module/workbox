# Nuxt.js Workbox generate module

[Nuxt.js](https://nuxtjs.org) module for generate [workbox](https://developers.google.com/web/tools/workbox) service worker.

**This is only for those use nuxt.js to generate static site**

## License

BSD 3 Clause License

## Usage

Add these in nuxt.config.js
```javascript
module.exports = {
    // other config is skipped
    modules: ['@404busters/nuxt-generate-workbox'],
    workbox: {
      plugin: {
        enabled: true,
      },
      generate: {
        file: 'sw.js',
        cacheId: 'workbox-cache',
        offlineGoogleAnalytics: true, // default false
      },
    },
}
```
